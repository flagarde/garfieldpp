#ifndef G_SENSORGPU_H
#define G_SENSORGPU_H

#ifndef __GPUCOMPILE__
#error GPU HEADER INCLUDED WITHOUT SETTING __GPUCOMPILE__
#endif

#include "MediumGPU.h"
#include "ComponentGPU.h"
#include "Garfield/Sensor.hh"

#endif
