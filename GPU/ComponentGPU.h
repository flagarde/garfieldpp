#ifndef G_ComponentGPU_H
#define G_ComponentGPU_H

#ifndef __GPUCOMPILE__
#error GPU HEADER INCLUDED WITHOUT SETTING __GPUCOMPILE__
#endif

#include "TetrahedralTreeGPU.h"
#include "MediumGPU.h"
#include "Garfield/Component.hh"


#endif
