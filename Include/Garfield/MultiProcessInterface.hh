#ifndef G_MULTIPROCESS_INTERFACE
#define G_MULTIPROCESS_INTERFACE

namespace Garfield {

  // Structure to determine how multi-processing is done
  enum class MPRunMode
  {
    Normal = 0,
    GPUExclusive
  };

}

#endif
